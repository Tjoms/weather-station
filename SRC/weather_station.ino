#include <stdint.h>
#include <RTClib.h>
#include "SparkFunBME280.h"
#include <LiquidCrystal.h>
#include "Wire.h"
#include <SD.h>
#include <EEPROM.h>

#define keyPress A0 //Buttons
#define chipSelect 10 //SD card
#define addressForFileName 0 //EEPROM ADDRESS

//Char array used for formatting data
char buffer [16];
//Set backlight to middle strenght
int backlight = 128;

int countLeftPress = 0;
int countRightPress = 0;

// Set maximum temperature the BME280 sensor can handle
double minTempC = 85;
//Set minimum temperature the BME280 sensor can handle
double maxTempC = -40;

unsigned long prevTime;
unsigned long currentTime;

enum button {
  UP,
  DOWN,
  RIGHT,
  LEFT,
  SELECT,
};

enum temperature {
  C,
  F,
  K
};

//Global sensor objects
File dataFile;
BME280 mySensor;
RTC_DS1307 rtc;
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);
DateTime now;

//Uncomment the line below to adjust a new time
// rtc.adjust(DateTime(2019, 11, 29, 14, 25, 0));

//A function that sets the "last edit" time when a file is updated.
//Is displayed if you look at the file from a computer
void dateTime(uint16_t* date, uint16_t* time) {
  now = rtc.now();

  // return date using FAT_DATE macro to format fields
  *date = FAT_DATE(now.year(), now.month(), now.day());

  // return time using FAT_TIME macro to format fields
  *time = FAT_TIME(now.hour(), now.minute(), now.second());
}

void setup()
{
  //***Driver settings********************************//
  mySensor.settings.commInterface = I2C_MODE;
  mySensor.settings.I2CAddress = 0x77;

  //***Operation settings*****************************//
  mySensor.settings.runMode = 3; //  3, Normal mode
  mySensor.settings.tStandby = 0; //  0, 0.5ms
  mySensor.settings.filter = 0; //  0, filter off
  //tempOverSample can be:
  //  0, skipped
  //  1 through 5, oversampling *1, *2, *4, *8, *16 respectively
  mySensor.settings.tempOverSample = 1;
  //pressOverSample can be:
  //  0, skipped
  //  1 through 5, oversampling *1, *2, *4, *8, *16 respectively
  mySensor.settings.pressOverSample = 1;
  //humidOverSample can be:
  //  0, skipped
  //  1 through 5, oversampling *1, *2, *4, *8, *16 respectively
  mySensor.settings.humidOverSample = 1;

  //***Initialize/begin*****************************//
  Serial.begin(9600);
  lcd.begin(16, 2);
  mySensor.begin();

  if (!SD.begin(chipSelect)) {
    Serial.println("initialization failed!");
    while (true);
  }
  if (!rtc.begin()) {
    Serial.println("initialization failed!");
    while (true);
  }
  Serial.println("initialization done.");

  //***Set initial conditions*****************************//
  analogWrite(3, backlight);
  updateMinMaxTemp();

  //Uncomment the line below if it is the first time running the program
  //Also the SD card cannot have previous files named DATAxxx.txt where xxxx is numbers
  saveFileNumber(0);
  //Uncomment if you want to read a file. Filenumber 1 is file DATA0001, filenumber 15 is file DATA0015
  // readSD(1); //Replace 1 with filenumber

  //Increment to create a new file
  saveFileNumber(getFileNumber() + 1);
  Serial.println("Creating new file... ");
  SdFile::dateTimeCallback(dateTime);
  Serial.println(getFileName());
}

void loop() {
  prevTime = millis();
  //Write to sd card every 10 sec
  while (prevTime + 10000 >= currentTime ) {
    updateDataEverySecond();
    checkForButtonPress();
    currentTime = millis();
  }
  writeToSD();
}
//---------------PRINT EVERY SECOND--------------
void updateDataEverySecond() {
  //This will be executed every second
  if (currentTime % 1000 == 0) {
    lcd.clear(); //Clear previous data from display
    lcdPrint_TempHumidityTime();
    lcdPrint_mBar();
  }
}
//---------------DECISION LOGIC--------------
void lcdPrint_TempHumidityTime() {
  temperature tempCase;
  //Print a case of a temperature depending on how many times the button have been pressed
  switch (countLeftPress % 3) {
    case 0:
      lcdPrint_TempC();
      tempCase = C;
      break;
    case 1:
      lcdPrint_TempF();
      tempCase = F;
      break;
    case 2:
      lcdPrint_TempK();
      tempCase = K;
      break;
  }
  //Uses the tempCase to convert the min and max temp to the wanted tempCase
  lcdPrint_HumidityOrMinMaxTemp(tempCase);
}

//--------------PRINT TEMP LOGIC-------------------
//A function that displays the temperature in Celsius
void lcdPrint_TempC() {
  double tempC = mySensor.readTempC();
  lcd.setCursor(0, 0);
  lcd.print(tempC, 1); //Display with 1 desimal
  lcd.setCursor(4, 0);
  lcd.print((char)223); //Degree symbol
  lcd.setCursor(5, 0);
  lcd.print("C");
}

//A function that displays the temperature in Celsius
void lcdPrint_TempK() {
  double tempK = getTemperatureK(mySensor.readTempC());
  lcd.setCursor(0, 0);
  lcd.print(tempK, 1); //Display with 1 desimal
  lcd.setCursor(5, 0);
  lcd.print((char)223); //Degree symbol
  lcd.setCursor(6, 0);
  lcd.print("K");
}

//A function that displays the temperature in Celsius
void lcdPrint_TempF() {
  double tempF = getTemperatureF(mySensor.readTempC());
  lcd.setCursor(0, 0);
  lcd.print(tempF, 1); //Display with 1 desimal
  lcd.setCursor(4, 0);
  lcd.print((char)223); //Degree symbol
  lcd.setCursor(5, 0);
  lcd.print("F");
}

//-------------PRINT mBAR LOGIC-------------------
//A function that displays the pressure in mBar
void lcdPrint_mBar() {
  //Converting from pascal to mBar
  int mBar = mySensor.readFloatPressure() / 100;
  lcd.setCursor(8, 0);
  sprintf(buffer, "%4dmBar", mBar);
  lcd.print(buffer);
}

//--------DECIDE PRINT HUMIDITY OR MIN MAX TEMP LOGIC----------
//A function that decides wether to display min and max temp
//or humidity and real time clock
void lcdPrint_HumidityOrMinMaxTemp(temperature tempCase) {
  countRightPress = countRightPress % 2;
  if (countRightPress == 0) {
    lcdPrint_Humidity();
    lcdPrint_HourMinSec();

  } else if (countRightPress == 1) {
    lcdPrint_MinMaxTemp(tempCase);
  }
}
//------------PRINT MIN/MAX TEMPERATURE LOGIC----------------
//A function which decides which type of temperature case its going to display
void lcdPrint_MinMaxTemp(temperature tempCase) {
  switch (tempCase) {
    case C:
      lcdPrint_MinMaxTemperatureC();
      break;
    case F:
      lcdPrint_MinMaxTemperatureF();
      break;
    case K:
      lcdPrint_MinMaxTemperatureK();
      break;
  }
  updateMinMaxTemp();
}

//A function that prints the min and max temp in Celsius
double lcdPrint_MinMaxTemperatureC() {
  lcdPrint_MinMaxTemp(minTempC, maxTempC);
}

//A function that prints the min and max temp in Fahrenheit
void lcdPrint_MinMaxTemperatureF() {
  //Convertion from Celsius to Fahrenheit
  double minTempF = (minTempC * 9 / 5) + 32;
  double maxTempF = (maxTempC * 9 / 5) + 32;
  lcdPrint_MinMaxTemp(minTempF, maxTempF);
}

//A function that prints the min and max temp in Kelvin
void lcdPrint_MinMaxTemperatureK() {
  //Convertion from Celsius to Kelvin
  double minTempK = minTempC + 273.15;
  double maxTempK = maxTempC + 273.15;
  lcdPrint_MinMaxTemp(minTempK, maxTempK);
}

//A function that prints the min and max temp case
void lcdPrint_MinMaxTemp(double minTemp, double maxTemp ) {
  lcd.setCursor(0, 1);
  lcd.print(minTemp, 1);
  lcd.setCursor(7, 1);
  lcd.print("/");
  lcd.setCursor(11, 1);
  lcd.print(maxTemp, 1);
}
//------------PRINT HUMIDITY LOGIC------------------------
//A function that displays the humidity in %
void lcdPrint_Humidity() {
  lcd.setCursor(0, 1);
  lcd.print((int)mySensor.readFloatHumidity()); //From float to int
  lcd.setCursor(2, 1);
  lcd.print("%");
}

//------------------BACKLIGHT ADJUST LOGIC-----------------------------------
void higherBacklighting() {
  //Map to 255 values
  if (backlight <= 255 - 16) { //take total value - the value you want to increment by
    backlight = (backlight + 16); // increment backlight, 16 levels of brightness
    analogWrite(3, backlight); //Writes a analog value to digital pin 3 which adjusts backlighting
  }
}

void lowerBacklighting() {
  //Map to 0-255 values
  if (backlight >= 0 + 16) { //take lowest value + the value you want to increment by
    backlight = (backlight - 16); // increment backlight
    analogWrite(3, backlight); //Writes a analog value to digital pin 3 which adjusts backlighting
  }
}

//-------------BUTTON PRESS LOGIC---------------------
//A function that checks if there is a button being pressed
void checkForButtonPress() {
  button button = keyPressed();
  switch (button) {

    case DOWN:
      lowerBacklighting();
      waitForButtonRelease(DOWN);
      break;

    case UP:
      higherBacklighting();
      waitForButtonRelease(UP);
      break;

    case LEFT:
      countLeftPress++;
      waitForButtonRelease(LEFT);
      break;

    case RIGHT:
      countRightPress++;
      waitForButtonRelease(RIGHT);
      break;

    case SELECT:
    //Do nothing
      break;
  }
}

//Checks the key being pressed and maps them 
//to an enum describing which button it is
button keyPressed() {
  int valueRead = analogRead(keyPress);
  switch (valueRead) {
    case 0 ... 100 :
      // stuff to do if RIGHT key pressed;
      return RIGHT;
    case 101 ... 160:
      // stuff to do if UP key pressed
      return UP;
    case 161 ... 340:
      // stuff to do if DOWN key pressed
      return DOWN;
    case 341 ... 510:
      // stuff to do if LEFT key pressed
      return LEFT;
    case 511 ... 750:
      // stuff to do if SELECT key pressed
      return SELECT;
  }
}

void waitForButtonRelease(button key) {
  while (keyPressed() == key) {
    //Wait for key release
  }
}

//-----------GET AND UPDATE TEMP LOGIC----------------------
// A function that returns the temperature in Kelvin
double getTemperatureK(double tempC) {
  double tempK = tempC + 273.15;
  return tempK;
}

// A function that returns the temperature in Fahrenheit
double getTemperatureF(double tempC) {
  double tempF = (tempC * 9 / 5) + 32;
  return tempF;
}
// A function that updates the min/max values if nessesary
void updateMinMaxTemp() {
  double currentTemp = mySensor.readTempC();
  if (currentTemp > maxTempC) {
    maxTempC = currentTemp;
  }
  if (currentTemp < minTempC) {
    minTempC = currentTemp;
  }
}

//--------------REAL TIME CLOCK LOGIC----------------------
//A function that prints the real time clock to the Serial monitor
void printRealTime() {
  now = rtc.now();
  Serial.print(now.year(), DEC);
  Serial.print(" / ");
  Serial.print(now.month(), DEC);
  Serial.print(" / ");
  Serial.print(now.day(), DEC);
  Serial.print(" / ");
  Serial.print(now.hour(), DEC);
  Serial.print(" / ");
  Serial.print(now.minute(), DEC);
  Serial.print(" / ");
  Serial.print(now.second(), DEC);
  Serial.println();
}

//A function that displays the real time clock
void lcdPrint_HourMinSec() {
  //Get current time
  now = rtc.now();
  lcd.setCursor(8, 1);
  //Format current time to wanted format which is hh:mm:ss
  sprintf(buffer, "%02u:%02u:%02u", now.hour(), now.minute() , now.second());
  lcd.print(buffer);
}

//--------------READ/WRITE SD CARD LOGIC-------------------------
//Read data from a file from an SD card
void readSD(int fileNumber) {
  //Format the fileName to be DATAxxxx where xxxx is a sequence of numbers
  String fileName = "DATA";
  sprintf(buffer, "%04d", fileNumber);
  fileName += buffer;

  //Open the file
  dataFile = SD.open(fileName);
  // if the file is available, read it:
  if (dataFile) {
    while (dataFile.available()) {
      Serial.write(dataFile.read());
    }
    dataFile.close();
  }
  // if the file could not be opened, pop up an error:
  else {
    Serial.print("error opening ");
    Serial.println(fileName);
  }
}

//A function that writes to a file on a SD card
void writeToSD() {
  //Sets the correct time for last edit file
  SdFile::dateTimeCallback(dateTime);
 
  String dataString = getDataString();
  String fileName = getFileName();

  //Adds data to "fileName" on the SD card if available
  dataFile = SD.open(fileName, FILE_WRITE);
  if (dataFile) {
    dataFile.println(dataString);
    dataFile.close();
    Serial.println(dataString);
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }
}

//A function that formats data to wanted format dd:mm:yyyy,hh:mm:ss,aaa,bbb,ccc
String getDataString() {
  String dataString = "";
  sprintf(buffer, "%02u/%02u/%04u,", now.day(), now.month() , now.year());
  dataString += buffer;

  sprintf(buffer, "%02u:%02u:%02u,", now.hour(), now.minute() , now.second());
  dataString += buffer;

  dataString += mySensor.readTempC();
  dataString += ",";

  dataString += int(mySensor.readFloatPressure() / 100);
  dataString += ",";

  dataString += int(mySensor.readFloatHumidity());
  return dataString;
}

//A fucntion that returns the filename formatted DATAXXXX
String getFileName() {
  String nameOfFile = "DATA";
  int fileNumber = getFileNumber();

  sprintf(buffer, "%04d", fileNumber);
  nameOfFile += buffer;

  return nameOfFile;
}

//---------------------SAVE/GET FILENUMBER IN EEPROM--------------------
//A function that saves the filenumber
void saveFileNumber(int fileNumber) {
  //Writes to an EEPROM address such that the filenumber can be 
  //gathered even after the arduino have been powered off
  EEPROM.update(addressForFileName, fileNumber);
}

//A functions that gets the filenumber
int getFileNumber() {
  //Gets the filenumber form the EEPROM location
  int fileNumber = (int) EEPROM[addressForFileName];
  return fileNumber;
}
